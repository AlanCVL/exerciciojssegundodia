var notas = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Alan",
        "turma": "A",
        "nota1": 2,
        "nota2": 10

    },
    {
        "nome": "Caio",
        "turma": "B",
        "nota1": 10,
        "nota2": 10
    },
    {
        "nome": "Karen",
        "turma": "C",
        "nota1": 10,
        "nota2": 9
    }
]

function media(notas){

    var classA = [];
    var classB = [];
    var classC = [];

    for( let i of notas){
        if(i.turma =='A'){
            classA.push(i);
        }
        else if(i.turma =='B'){
            classB.push(i);
        }
        else{
            classC.push(i);
        }
    }
    

    let mediaA = (classA[0].nota1 + classA[0].nota2)/2;
    let infoA = classA[0];
    
    for( let k in classA){
        res = ( classA[k].nota1 + classA[k].nota2 )/2;
        if (res > mediaA){
            mediaA = res;
            infoA = classA[k];
        }
    }
    console.log(`O aluno ${infoA.nome} teve a media mais alta da turma ${infoA.turma} com média de ${mediaA}`);


    let mediaB = (classB[0].nota1 + classB[0].nota2)/2;
    let infoB = classB[0];
    
    for( k in classB){
        res = ( classB[k].nota1 + classB[k].nota2 )/2;
        if (res > mediaB){
            mediaB = res;
            infoB = classB[k];
        }
    }
    console.log(`O aluno ${infoB.nome} teve a media mais alta da turma ${infoB.turma} com média de ${mediaB}`);



    let mediaC = (classC[0].nota1 + classC[0].nota2)/2;
    let infoC = classC[0];

    for( k in classC){
        res = ( classC[k].nota1 + classC[k].nota2 )/2;
        if (res > mediaC){
            mediaC = res;
            infoC = classC[k];
        }
    }
    console.log(`O aluno ${infoC.nome} teve a media mais alta da turma ${infoC.turma} com média de ${mediaC}`);

}
media(notas);